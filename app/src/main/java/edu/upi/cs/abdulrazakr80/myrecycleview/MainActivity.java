package edu.upi.cs.abdulrazakr80.myrecycleview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.materi){
            startActivity(new Intent(this, MainActivity.class));
        } else if (item.getItemId() == R.id.pilihkelas) {
            startActivity(new Intent(this, HomeActivity.class));
        } else if (item.getItemId() == R.id.profil) {
            startActivity(new Intent(this, ProfilActivity.class));
        }

        return true;
    }
}